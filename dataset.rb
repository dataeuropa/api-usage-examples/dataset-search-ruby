require 'uri'
require 'net/http'
require 'openssl'

# retrieve the information for a datasts in the catalogue 'EU institutions'
# the dataset has the ID 'g3ebxpyzj2lulbjqwkhg'

url = URI('https://data.europa.eu/api/hub/search/datasets/g3ebxpyzj2lulbjqwkhg')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Get.new(url)


response = http.request(request)
puts response.read_body



