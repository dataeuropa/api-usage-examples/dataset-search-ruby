require 'uri'
require 'net/http'
require 'openssl'
# retrieve the information for the catalogue 'EU institutions', which has the ID 'european-union-open-data-portal'

url = URI('https://data.europa.eu/api/hub/search/catalogues/european-union-open-data-portal')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Get.new(url)


response = http.request(request)
puts response.read_body


