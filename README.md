# Ruby examples for data.europa.eu Search API

## Prerequisites

1. Ruby

## Examples

The examples can be run with `ruby {filename}`, e.g. `ruby search.rb`

### Search

To search for a term, the Endpoint `/search` has to be used with the parameter `q`.

[search.rb](search.rb):
```ruby
require 'uri'
require 'net/http'
require 'openssl'
# search for the term 'Covid 19'
url = URI('https://data.europa.eu/api/hub/search/search?q=covid%2019')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Get.new(url)


response = http.request(request)
puts response.read_body

```

### Search in specific catalogue

To limit the search to a specific catalogue, the `facets` query parameter has to be used with the catalog facet.
Additionally, to enable the facet search, the query parameter `filter` has to be set to `datasets`:

[searchWithFilter.rb](searchWithFilter.rb):
```ruby
require 'uri'
require 'net/http'
require 'openssl'
# search for the term 'Covid 19' in the catalogue 'EU institutions', which has the id 'european-union-open-data-portal'

url = URI('https://data.europa.eu/api/hub/search/search?q=covid%2019&facets=%7B%22catalog%22%3A%5B%22european-union-open-data-portal%22%5D%7D&filter=dataset')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Get.new(url)


response = http.request(request)
puts response.read_body


```

### Retrieve a specific catalogue

To retrieve the information for a specific catalogue, the endpoint `/catalogues` has to be used with the catalogue ID in the path.

[catalogue.rb](catalogue.rb):
```ruby
require 'uri'
require 'net/http'
require 'openssl'
# retrieve the information for the catalogue 'EU institutions', which has the ID 'european-union-open-data-portal'

url = URI('https://data.europa.eu/api/hub/search/catalogues/european-union-open-data-portal')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Get.new(url)


response = http.request(request)
puts response.read_body
```

### Retrieve a specific dataset

To retrieve the information for a specific dataset, the endpoint `/datasets` has to be used with the dataset ID in the path.

[dataset.rb](dataset.rb):
```ruby
require 'uri'
require 'net/http'
require 'openssl'

# retrieve the information for a datasts in the catalogue 'EU institutions'
# the dataset has the ID 'g3ebxpyzj2lulbjqwkhg'

url = URI('https://data.europa.eu/api/hub/search/datasets/g3ebxpyzj2lulbjqwkhg')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Get.new(url)


response = http.request(request)
puts response.read_body
```
