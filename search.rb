require 'uri'
require 'net/http'
require 'openssl'
# search for the term 'Covid 19'
url = URI('https://data.europa.eu/api/hub/search/search?q=covid%2019')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Get.new(url)


response = http.request(request)
puts response.read_body
