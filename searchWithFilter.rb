require 'uri'
require 'net/http'
require 'openssl'
# search for the term 'Covid 19' in the catalogue 'EU institutions', which has the id 'european-union-open-data-portal'

url = URI('https://data.europa.eu/api/hub/search/search?q=covid%2019&facets=%7B%22catalog%22%3A%5B%22european-union-open-data-portal%22%5D%7D&filter=dataset')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Get.new(url)


response = http.request(request)
puts response.read_body

